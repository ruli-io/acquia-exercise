<?php

namespace Drupal\embargo_content\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\Cookie;

/**
 * Authenticate Form.
 */
class AuthenticateForm extends FormBase {

  /**
   * Custom form ID
   * @return string
   */
  public function getFormId() {
    return 'embargo_content_authenticate_form';
  }

  /**
   * Build form array
   * @return $form
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#required' => TRUE,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Passwords
    $node = \Drupal::routeMatch()->getParameter('node');
    $correctPassword = $node->get('field_embargo_password')->getString();

    // If mismatch return error
    if ( $form_state->getValue('password') !== $correctPassword ) {
      $form_state->setErrorByName('password', $this->t('Incorrect password, please try again.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $session = \Drupal::service('user.private_tempstore')->get('embargo_content');
    $session->set('embargo_auth', 'auth');

  }
}
