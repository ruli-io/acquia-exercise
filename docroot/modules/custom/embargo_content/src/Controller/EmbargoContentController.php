<?php

namespace Drupal\embargo_content\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Embargo Content Controller
 */
class EmbargoContentController extends ControllerBase {

  /**
   * Generates a random password based on criteria.
   * @param  $length
   * @param  $count
   * @param  $characters
   * @return $passwords
   */
  public function generateRandomPassword($length, $count, $characters) {

    $symbols = [];
    $passwords = [];
    $used_symbols = '';
    $pass = '';

    // an array of different character types
    $symbols['lower_case'] = 'abcdefghijklmnopqrstuvwxyz';
    $symbols['upper_case'] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $symbols['numbers'] = '1234567890';
    $symbols['special_symbols'] = '!?~@#-_+<>[]{}';

    // get characters types to be used for the passsword
    $characters = explode(',', $characters);

    // build a string with all characters
    foreach ($characters as $key => $value) {

      $used_symbols .= $symbols[$value];

    }

    $symbols_length = strlen($used_symbols) - 1;

    // Randomize characters
    for ($p = 0; $p < $count; $p++) {

        $pass = '';

        for ($i = 0; $i < $length; $i++) {

          $n = rand(0, $symbols_length);
          $pass .= $used_symbols[$n];

        }

        $passwords[] = $pass;
    }

    return $passwords;
  }

  /**
   * Check if a date is in the future
   * @param  $value
   * @return boolean
   */
  public function isDateInFuture($value){

    $date = new \DateTime($value);
    $now = new \DateTime();

    return $date > $now;
  }

}
