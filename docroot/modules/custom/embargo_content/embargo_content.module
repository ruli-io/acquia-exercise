<?php

use Drupal\embargo_content\Controller\EmbargoContentController;

/**
 * Node presave hook
 * @param $node
 */
function embargo_content_node_presave($node) {

  $Controller = new EmbargoContentController;
  $nodeType = $node->bundle();

  // ONLY for "page" content type
  if($nodeType == 'page'){

    // Get embargo date value
    $embargoDate = $node->get('field_embargo_date')->getString();

    // Proceed if it's not an empty string
    if ($embargoDate != ''){

      // Generate random password ONLY if date is in the future
      if( $Controller->isDateInFuture($embargoDate) ){

        $randomPassword = $Controller->generateRandomPassword(10, 1, 'lower_case,upper_case,numbers,special_symbols');

        // Add random password to node
        $node->set('field_embargo_password', $randomPassword);

      }
    }
  }

}

/**
 * Node preprocess hook
 * @param $node
 */
function embargo_content_preprocess_node(&$vars) {

  $node = $vars['node'];
  $nodeType = $node->bundle();
  $Controller = new EmbargoContentController;

  // ONLY for "page" content type
  if($nodeType == 'page'){

    // Prevent page cache to allow session updates
    $vars['#cache']['max-age'] = 0;

    // Get embargo date value
    $embargoDate = $node->get('field_embargo_date')->getString();

    // Authorize content by default
    $vars['authorized_content'] = true;

    // Proceed if it's not an empty string
    if ($embargoDate != ''){

      // Custom logic if date is in future
      if( $Controller->isDateInFuture($embargoDate) ){

        $user = \Drupal::currentUser()->getRoles();

        // Admin role - Show password and content
        if( in_array('administrator', $user) ){

          $vars['show_password'] = true;

        // Anonymous role - show auth form or content if authorized
        }else if( in_array('anonymous', $user) ){

          // Check embargo_auth
          $session = \Drupal::service('user.private_tempstore')->get('embargo_content');
          $data = $session->get('embargo_auth');

          if( $data != 'auth' ){

            $vars['auth_form'] = \Drupal::formBuilder()->getForm('Drupal\embargo_content\Form\AuthenticateForm');
            $vars['authorized_content'] = false;

          }
        }
      }
    }
  }

}
