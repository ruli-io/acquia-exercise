# Acquia Showcase - Exercise
---
## User Story

As a site owner, I want to release embargoed content to a small group of users who will have to supply a password to access it so that I can have a limited release of content before it comes accessible to the world.

## Acceptance Criteria

- Content creator will be able to set an Embargo Release date on a piece of content.
- If the date is in the future, the system should generate a unique password which must be supplied by an anonymous user in order to access the content.
- If no embargo release date is set or the release date has passed, the content should be available to all users able to see published content.
